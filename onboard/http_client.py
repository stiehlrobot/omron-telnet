from urllib import request
from urllib.error import HTTPError, URLError


server_url_rootpath = 'SERVER_BASE_URL_HERE'

def send_request(full_url):
    try:
        resp = ""
        with request.urlopen(full_url) as req:
            resp = req.read(300).decode()
        return resp
    except URLError as er:
        print(er)
    except HTTPError as err:
        print(err)

def server_response_nominal():
    response = send_request(server_url_rootpath)
    if 'Home' in response:
        print("Connection test successful")
        True
    else:
        False
        
def get_job_in_queue():
    '''Gets the topmost object from the queue'''
    full_url = create_url(server_url_rootpath,'jobs/get/')
    return send_request(full_url)
    
def remove_a_job_from_the_queue():
    '''Removes the FI job in the queue'''    
    full_url = create_url(server_url_rootpath,'jobs/remove')
    return send_request(full_url)
    
def add_job_with_name(job_name):
    '''Add job_name to the end of the queue'''
    full_url = create_url(server_url_rootpath,'jobs/add/?name={}'.format(job_name))
    return send_request(full_url)
    
def create_url(rootpath, custom_ending):
    '''forms full urls'''
    return rootpath + custom_ending

if __name__=='__main__':
    
    if server_response_nominal():
        print("Server is online and responding!")
    job = get_job_in_queue()
    print(f"Got job {job} from queue")

    remove_a_job_from_the_queue()
    add_job_with_name("job_name")
    get_job_in_queue()
    