import time
import sys
from telnetlib import Telnet
from datetime import datetime
from pytz import timezone
import pytz
import telnet_creds
#remember to populate telnet creds with your ip and telnet pw

def login_routine():
    '''Login routine to omron ld 60, timeout 10 seconds'''
    print('Initiating login routine..')
    try:        
        tn = Telnet(telnet_creds.get_robot_ip(), 7171)
        tn.read_until(b'Enter password: \r\n', 10)
        tn.write(b'{}\n'.format(telnet_creds.get_robot_pw()))
        tn.read_until(b'Welcome to the server.\r\n', 10)
        
        return tn
        
    except:        
        tn.close()
        return None

def send_command(tn, msg):
    '''Send a bytestring via open telnet connection'''
    print("Sending", msg)
    tn.write(msg.encode('utf-8'))

def utcnow():
    utc = pytz.utc
    helsinki = timezone('Europe/Helsinki')
    utc_dt = datetime(datetime.now(), tzinfo=utc)
    loc_dt = utc_dt.astimezone(helsinki)    
    fmt = '%Y-%m-%d %H:%M:%S %Z%z'
    
    return loc_dt.strftime(fmt)

def main():
    conn = login_routine()
    if conn == None:
        print("Problem logging in.")
        sys.exit(0)
    else:
        print(utcnow())
        print("Login success!")
        print("*"*30)
        send_command(conn, 'dotask sayInstant “I am online and ready”\n')
        time.sleep(2)                
        conn.close()    

if __name__=='__main__':
    main()
    
