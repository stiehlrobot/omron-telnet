"""class for scheduling events for robot"""
from datetime import datetime, timedelta
import time

class CommandScheduler():
    
    def __init__(self):
        self.scheduled_commands = {"2020-10-03 13:25:00": "patrolOnce route A"}        

    def utc_now(self):
        '''Returns formatted localtime'''
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    def activate_scheduler(self):
        '''Activates main scheduler loop'''
        try:
            while True:
                t_now = self.utc_now()
                if t_now in self.scheduled_commands.keys():
                    print("TIME MATCH!")
                    print("Execute: ",self.scheduled_commands[t_now])
                    time.sleep(1)
        except KeyboardInterrupt:
            sys.exit(0)
            break       

    def schedule_command(self, timestamp, command):
        '''Schedule a command'''
        overlapping_entries = False
        
        dt_timestamp = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
        dt_t = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
        for t in self.scheduled_commands.keys():
            dt_t = datetime.strptime(t, "%Y-%m-%d %H:%M:%S")
            time_delta = (dt_timestamp - dt_t)
            total_seconds = time_delta.total_seconds()
            print(total_seconds)
            if total_seconds >= -60 and total_seconds <= 60:                
                overlapping_entries = True
                break
            else:
                continue
                
        if overlapping_entries:
            print("This timeslot already used. Try scheduling another time.")
        else:
            self.scheduled_commands[timestamp] = command
            print("Command: ", command, " scheduled for time: ", timestamp)

    def get_scheduled_commands(self):
        '''returns scheduled command dict'''
        return self.scheduled_commands
    
def main():
    cs = CommandScheduler()
    cs.schedule_command("2020-10-03 14:40:25", "goTo goal A")
    cs.schedule_command("2020-10-03 14:41:30", "goTo goal A")
    cs.activate_scheduler()
    
if __name__=='__main__':
    main()