'''module running core process'''
import sys
import http_client
import telnet_socket as telnet
import time
import re

"""
SECURITY
When designing APIs (both general-purpose or public interfaces as well as those that are do-main- or application-specific), avoid exposing methods or endpoints that consume strings in languages that embed both control and data. Prefer instead to expose, for example, methods or endpoints that consume structured types that impose strict segregation between data and control information.

"""
#Robot map broken down into the following areas
floor_states_koks = {"koks floor 0 lobby": (-36000, -13000, 42900, 56000),
                "koks floor 1": (-60000, -26000, -43000, 30500),
                "koks elevator":(-42500, -28400, 30501, 43300),
                "koks floor 0 tunnel":(-20000, 46000, -22000, 43000)}


ROBOT_STATE_ON_JOB = False
JOB_COMPLETION_TIMEOUT_SEC = 600
JOB_CONFIRMATION_TIMEOUT_SEC = 10
STATUS_QUERY_TIMEOUT_SEC = 3
WEBSERVER_JOB_QUERY_INTERVAL_SEC = 5

CURRENT_ROBOT_LEVEL = None
ROBOT_POS_X = None
ROBOT_POS_Y = None

CMD_BEACON_ON = "outputOn discovalo"
CMD_BEACON_OFF = "outputOff discovalo"

class JobConfirmationException(Exception):
    pass

class JobCompletionTimeoutException(Exception):
    pass

class StatusResponseTimeout(Exception):
    pass

class RouteFormatValidationError(Exception):
    pass

def _validatrix(str_to_validate):
    '''Security feature used to validate input format using regex'''
    matching = '[a-z]{6}[0-9]'
    p = re.compile(matching)
    m = p.match(str_to_validate)
    if m:
        print("Job format validated!")
        return True
    else:
        print("Error: RouteFormatValidationError")
        return False
        

def _get_routes():
    routes = {"reitti1": "patrolOnce reitti1",
              "reitti2": "patrolOnce reitti2"}
    return routes

def init_http():
    '''Initialize modules'''
    #Test connection to webserver
    if not http_client.server_response_nominal():
        print("Trouble reaching  job queue server, aborting..")
        sys.exit(0)
    print("Connection test to webserver succesful!")   

def in_bounds(value, min_threshold, max_threshold):
    if min_threshold <= value <= max_threshold:
        return True
    return False

def determine_robot_level(pos_x, pos_y):
    global CURRENT_ROBOT_LEVEL, floor_states_koks
    '''determine robot level based on X and Y pos'''
    for key, val in floor_states_koks.items():
        x1, x2, y1, y2 = val
        if in_bounds(pos_x, x1, x2) and in_bounds(pos_y, y1, y2):
            CURRENT_ROBOT_LEVEL = key
            print("Currently at: ",key)
            break    

def connect_to_robot_and_determine_robot_current_floor(conn):

    #Query LD60 for X and Y position
    print("Waiting for status response from LD60..")
   
    try:
        telnet.send_command(conn, 'onelinestatus\n')
        conn.read_until(b"Status: ", STATUS_QUERY_TIMEOUT_SEC)
        status_contents = conn.read_very_eager().decode()
        print(status_contents)
        status_msg_end_part = status_contents.split("Location: ")
        coordinates = status_msg_end_part[1].split(" ")
        print("X should be: {} \n Y should be: {} \n ".format(coordinates[0], coordinates[1]))
        ROBOT_POS_X = int(coordinates[0])
        ROBOT_POS_Y = int(coordinates[1])
        
    except StatusResponseTimeout as err:
        print("Status response exception!")
        
    #Determine robot level based on robot X and Y co-ordinates
    if ROBOT_POS_X is not None and ROBOT_POS_Y is not None:
        determine_robot_level(ROBOT_POS_X,ROBOT_POS_Y)
    return conn

def main():
    init_http()
    
    while True:
        #every 5 seconds ping the server for new job, job/get, validate job        
        
        job = http_client.get_job_in_queue().strip()       

        if job == "No jobs in the queue!":
            pass
        else:
            print("Received new job: ", job)
            if not _validatrix(job):
                print("Skipping navigation to goal. Reason: job format not validated")
                continue
            
            conn = telnet.login_routine()
            conn = connect_to_robot_and_determine_robot_current_floor(conn)
            if job in _get_routes().keys():
                #specify which route to choose by prefixing yla or ala meaning upper or lower to the robot job name
                if CURRENT_ROBOT_LEVEL == "koks floor 0 lobby" or CURRENT_ROBOT_LEVEL == "koks floor 0 tunnel":

                    job = _get_routes()[job] + "ala\n"                  
                else:
                    job = _get_routes()[job] + "yla\n"                    
            else:
                print("No recognized job.")                   
            
            print("Relaying job to LD 60")
            print(job)
            telnet.send_command(conn, job)
            #Awaits for confirmation, timeout 10 seconds
            try:
                print("Waiting for job confirmation from LD 60..")
                conn.read_until(b'Patrolling route\r\n', JOB_CONFIRMATION_TIMEOUT_SEC)
                
            except JobConfirmationException as err:
                print("No confirmation for starting job, aborting..", job)
                conn.close()
                continue
            
            try:
                #find a way to 
                conn.read_until(b'Finished patrolling route\r\n', JOB_COMPLETION_TIMEOUT_SEC)
                print("Finished patrolling route: ", job)
                http_client.remove_a_job_from_the_queue()
                
            except JobCompletionTimeoutException as err:
                print("Job completion timeout of ",JOB_COMPLETION_TIMEOUT_SEC, " exceeded!" )
                print("Closing connection.")
                conn.close()
                continue
            
            conn.close()
        print("Sleeping 5 seconds..")
        time.sleep(WEBSERVER_JOB_QUERY_INTERVAL_SEC)
    
if __name__=='__main__':
    main()
