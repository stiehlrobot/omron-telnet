'''core process'''
import http_client
from gpiozero import Button
import time
import sys
import signal
import keyboard

BUTTON_TYPE = None

BUTTON_GPIO_PIN = 2 #todo define

def init():
    if not http_client.server_response_nominal():
        print("Trouble with http request, aborting..")
        sys.exit(0)
        
def send_robot_call_request_to_server(command):
    print("Button press detected. Sending robot call request to server.")
    http_client.add_job_with_name(command)
    
def main(button_type):
    init()
    if button_type == 'usb':
        # Bounce time = this is the length of time (in seconds) that the component will ignore changes in state after an initial change.
        button = Button(BUTTON_GPIO_PIN, bounce_time=120)
        button.when_pressed = send_robot_call_request_to_server
    while True:
        if keyboard.is_pressed('o'):
            print("Robot call button O pressed")
            send_robot_call_request_to_server("patrolOnce route1")
            #bounce time
            time.sleep(120) #bounce time
        elif keyboard.is_pressed('t'):
            print("Robot call button T pressed")
            send_robot_call_request_to_server("patrolOnce route2")
            time.sleep(120) #bounce time


    
if __name__=='__main__':
    if len(sys.argv) != 2:
        #currently only usb button press implemented
        print("Usage: python3 core.py [usb/gpio]")
        sys.exit(0)
    else:
        main(sys.argv[len(sys.argv) - 1])
