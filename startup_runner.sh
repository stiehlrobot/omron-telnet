#Bash script to be run on startup

#1 Pull newest version from git
echo "Performing git operations to get newest version from master branch"
cd /home/pi/omron-telnet/omron-telnet
git fetch --all
git checkout -b backup-master
git reset --hard origin/master

#2 Start virtualenv
echo "Activating virtualenv"
cd ..
pwd
source /home/pi/omron-telnet/env/bin/activate

#3. Start buttoncaller core
echo "Attempting to start buttoncaller core"
cd omron-telnet/buttoncaller
sudo python3 core.py usb
