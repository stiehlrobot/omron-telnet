# Omron Telnet

This repo contains
1) buttoncaller - to be used on an external raspberry pi attached to a callbutton
2) onboard - to be used on an onboard raspberry pi connected to a omron ld 60 robot

## 1. Buttoncaller - core functionalities

This is a simple program meant to be run on a raspberry pi connected to a pushbutton which can be used to add a new job to the robots job queue hosted on a webserver.

- Listens for button presses with bounce time 
- Sends a http request to a url when button press detected

## 2. Onboard - core functionalities:

This is a middleware sw designed to run on a Raspberry Pi onboard the Omron LD 60 robot for prototyping purposes.

- fetching jobs from server using http requests
- initating telnet connection to Omron LD 60 robot
- querying robot position in x,y
- performing robot position designation to specific area
- choosing best route for a robot using a multi-level map 

Extensions
- The software also easily allows for control commands to be sent to the robot when entering a specific area allowing for example the activation of a warning light, robot IO pins or other similar behaviors.



## Project funding

This software was created for the Small Waste Logistics in a Hospital Environment project funded by the EU regional development fund. 

Project video - https://www.youtube.com/watch?v=oSubJQbgqAg
